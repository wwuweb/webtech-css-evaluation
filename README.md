# WebTech Front-End Developer Practical

The purpose of this task is to showcase your current knowledge of HTML and CSS.
This represents a fairly typical task in the office where you are provided an
image of what a website should look like, and you have to convert it code.

## Getting started

1. Clone or [download](https://bitbucket.org/wwuweb/webtech-css-evaluation/get/master.zip)
   the files into a workspace on your computer.
2. mockup.png contains the design you will use as a reference for styling
   index.html. sample_solution.png is provided as an example of an acceptable
   result.
3. Open up index.html with a web browser to see the rendered HTML.
4. Open css/main.css in your favorite code editor and start crafting your
   solution.

## Submitting your solution

Once you are happy with your solution, feel free to send us the CSS file or a
link to a repository on GitHub or Bitbucket.

### Caveats

* Your solution must render correctly in the latest stable version of either
  Firefox, Chrome, or Edge. You do not need to test in all browsers as we will
  evaluate only the most accurate rendering of your code.
* You must not alter the provided HTML in any way. This includes adding
  additional stylesheets or scripts. However, you may use `@import` directives
  in main.css to organize your code.
* Your solution does not need to be pixel-perfect. However, the relative sizing
  of text, elements, and margins/padding should be consistent.
* For compatibility, please limit your font-family selections to the generic
  families: "serif", "sans-serif", "monospace", etc.
* You are more than welcome to use Sass, Less, or other CSS pre-processors. If
  you do, please provide both the original source file and your compiled
  solution.
* A typical solution should be about 100-200 lines of CSS. A responsive solution
  is not required as we have not provided a responsive mock-up. However, feel
  free to make your solution responsive if you wish.

## Useful tools

* A great code editor available on all platforms: [Atom.io](https://atom.io/)
